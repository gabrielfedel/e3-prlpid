# Load iocStats
require iocStats 3.1.16

epicsEnvSet("LOCATION", "prltest")
epicsEnvSet("IOCNAME", "IOC_TEST")
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=IOC_TEST")


# Load ECMC
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="IOC_TEST")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

require ecmccfg Julen_prl

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=6.0.1,EthercatMC_VER=3.0.2,stream_VER=2.8.10"

##############################################################################
## Configure hardware:

# first TCB
$(SCRIPTEXEC) $(ecmccfg_DIR)ecmcTCB_0.cmd
$(SCRIPTEXEC) $(ecmccfg_DIR)ecmcTCB_1.cmd
$(SCRIPTEXEC) $(ecmccfg_DIR)ecmcTCB_2.cmd

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

require prlpid, master

# Set paramaters for 1st PID
epicsEnvSet("P",    "$(IOC)")
epicsEnvSet("R",    "1")
# 2 temperature input channels
epicsEnvSet("INPA",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s31-EL3202-0010-AI1")
epicsEnvSet("INPB",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s31-EL3202-0010-AI2")
# output channel
epicsEnvSet("OUT", "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s22-EL2502-BO2")

iocshLoad("$(prlpid_DIR)/pid_2ch.iocsh")


# Set paramaters for 2st PID
epicsEnvSet("P",    "$(IOC)")
epicsEnvSet("R",    "2")
# 2 temperature input channels
epicsEnvSet("INPA",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s47-EL3202-0010-AI1")
epicsEnvSet("INPB",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s47-EL3202-0010-AI2")
# output channel
epicsEnvSet("OUT", "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s38-EL2502-BO2")

iocshLoad("$(prlpid_DIR)/pid_2ch.iocsh")

